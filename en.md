# Zaurux

#### What is Zaurux?
Zaurux is an OS which has some preinstalled programs,
a microkernel and documentation. Also it has Minian shell.

## Chapter 1: Pre-History
Idea of Zaurux appeared in my mind in summer of 2022, when I decided,
that i want to create my own operating system. I didn't knew how
to write programs, I even didn't knew about free software.

##### GNU/Linux and Free software
In autumn of 2022, I've read about what GNU/Linux is and about GNU project.
I tried fedora and saw, that it is very good.

##### My first program
In the same time, I've learned C++ language, it was really good, but in January 2023,
I stopped learning it and stopped programming for a while...

## Chapter 2: Andesaurux, the microkernel

##### Back to programming, birth of Zaurux.
In October 2023, I returned to the programming, I saw that C++ is too difficult and started
to learn Java, but sooner, I saw that it is very bad. I started learning C and continued
my TolyaDOS project(it was just if-else game, not real DOS). I rewrote it in C instead of C++.
I was developing it until January 7 2024, when I thought, that I'm ready for developing a real
OS and started developing Andesaurux(was innitialy a kernel).

##### Initial plans
I wanted to create a kernel called Andesaurux and distro for it called AndeOSaurus, I created a
simple C library(for what?!). Also I created a unistd.h(for what?!) with syscalls in it.

##### Kernel -> Full OS
Sooner, I decided that creating a kernel and distro for it is bad idea and I started to include
apps to Andesaurux, because kernel is useless by itself.

## Chapter 3: Zaurux as it looks now.
I renamed Andesaurux to Zaurux, because it is simpler and more original. I am still developing
Zaurux and in May it will boot(I will party all day after it).

### Why i switched to Codeberg?
Because on GitHub I can easily get banned, also, it is controlled by Microsoft which doesn't
like free(libre) software and I think, it's a trap. Also GitHub is proprietary. There are
GitHub mirrors of Zaurux, but, they are read-only. You cannot make pull requests for it. To
contribute, please, switch to CodeBerg.

Zaurux-Book © 2024 by AnatoliyL is licensed under Attribution-ShareAlike 4.0 International.